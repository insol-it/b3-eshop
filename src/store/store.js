
import { createStore } from 'vuex'

const store = createStore({
  state () {
    return {
      count: 0,
      products : [],
      user : null,
      cart : []
    }
  },
  getters: {
      products(state) {
          return state.products;
      },
      user(state) {
        return state.user;
      },
      cart(state) {
        return state.cart;
      },
  },
  mutations: {
    increment (state) {
      state.count++
    },
    products (state, value) {
        state.products = value;
    },
    user (state, value) {
      state.user = value;
    },
    cartadd (state, value) {
      let newCart = [];
      newCart = newCart.concat(state.cart);
      newCart.push(value);
      state.cart = newCart;
    },
    cartremove (state, value) {
      let newCart = [];
      newCart = newCart.concat(state.cart);
      newCart.splice(value, 1);
      state.cart = newCart;
    }
  },
  actions: {
      loadProducts (context) {
          fetch('/products.json')
            .then(raw => raw.json())
            .then(json => {
                context.commit('products',json.products);
            })
      }
  }
})

export default store;